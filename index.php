<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$db_hostname = 'localhost';
$db_username = 'root';
$db_password = '';
$db_database = 'searchFormDB';

// Database Connection String
$con = mysqli_connect( $db_hostname, $db_username, $db_password );
if ( ! $con )
{
	die( 'Could not connect: ' . mysqli_connect_error() );
}

if ( ! mysqli_select_db( $con, $db_database ) ) {
	die( 'Could not connect to database' );
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Search Form Example</title>
	<style>
		table, th, td {
			border: 1px solid black;
		}
	</style>
</head>
<body>

<form action="" method="post" accept-charset="utf-8">

	<div>
		<label for="full_name">
			Full Name
		</label>
		<input type="text" name="full_name" id="full_name" value="<?php echo htmlspecialchars( $_POST['full_name'] ?? '' ) ?>">
	</div>
	<div>
		<label for="city">
			City
		</label>
		<input type="text" name="city" id="city" value="<?php echo htmlspecialchars( $_POST['city'] ?? '' ) ?>">
	</div>
	<div>
		<label for="job">
			Job
		</label>
		<input type="text" name="job" id="job" value="<?php echo htmlspecialchars( $_POST['job'] ?? '' ) ?>">
	</div>

	<button type="submit" name="button">Search</button>

</form>
<br><br>

<?php
	$full_name = $city =  $job = '';
	$full_name_exist = $city_exist = $job_exist = 1;

	$result = null;
	if ( isset( $_POST['button'] ) ) { // the form submitted

		$full_name 	= mysqli_real_escape_string( $con, $_POST['full_name'] );
		$city 			= mysqli_real_escape_string( $con, $_POST['city'] );
		$job 				= mysqli_real_escape_string( $con, $_POST['job'] );

		$full_name_exist 	= $full_name ? 0 : 1;
		$city_exist 			= $city ? 0 : 1;
		$job_exist 				= $job ? 0 : 1;

	  if (
			$full_name
		  || $city
		  || $job
	  ){
		  $sql = <<<SQL
SELECT
	*
FROM
	users
WHERE
	(
		'1' = {$full_name_exist}
		OR fullName = '{$full_name}'
	)
	AND
	(
		'1' = {$city_exist}
		OR city = '{$city}'
	)
	AND
	(
		'1' = {$job_exist}
		OR job = '{$job}'
	)
SQL;

		  $result = mysqli_query( $con, $sql );
	  }
	}

	if ( $result && mysqli_num_rows( $result ) > 0 ) {
		?>
		<table>
			<tr>
				<th><strong>ID</strong></th>
				<th><strong>Full Name</strong></th>
				<th><strong>City</strong></th>
				<th><strong>Job</strong></th>
			</tr>
		<?php
			while ( $row = mysqli_fetch_array( $result ) ) {
		?>
				<tr>
					<td><?php echo $row['id'];?></td>
					<td><?php echo $row['fullName'];?></td>
					<td><?php echo $row['city'];?></td>
					<td><?php echo $row['job'];?></td>
				</tr>
		<?php
			}
		?>
		</table>
<?php
	}
	else {
		if (
			$full_name
			|| $city
			|| $job
		){
			?>
				<div>No results found</div>
			<?php
		}
		else if ( isset( $_POST['button'] ) ) {
			?>
				<div>Please fill the search fields</div>
			<?php
		}
  }
?>

</body>
</html>
